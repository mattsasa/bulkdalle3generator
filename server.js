import OpenAI from "openai"
import { createServer } from "http"
import { WebSocketServer, WebSocket } from "ws"
import express from "express"
import fs from "fs"
import axios from "axios"
import { google } from "googleapis"

const port = process.env.PORT
const hostname = process.env.HOSTNAME

const app = express()
const server = createServer(app)
const wss = new WebSocketServer({ server })

const openai = new OpenAI()

const redirect_uri =
    hostname == "localhost"
        ? `http://localhost:${port}/dalleGeneratorUpdateGoogleApiTokens`
        : `https://${hostname}/dalleGeneratorUpdateGoogleApiTokens`

const oauth2Client = new google.auth.OAuth2(process.env.GOOGLE_CLIENT_ID, process.env.GOOGLE_CLIENT_SECRET, redirect_uri)
const gTokens = JSON.parse(fs.readFileSync("storage/tokens.json", "utf8"))
oauth2Client.setCredentials(gTokens)

const saveTokens = (tokens) => {
    fs.writeFileSync("storage/tokens.json", JSON.stringify(tokens))
}

// Generate a url that asks permissions for the Photos library
const SCOPES = ["https://www.googleapis.com/auth/photoslibrary"]
const url = oauth2Client.generateAuthUrl({
    access_type: "offline",
    scope: SCOPES,
    prompt: "consent",
})
console.log("URL to refresh google tokens: ")
console.log(url, "\n")

app.get("/dalleGeneratorUpdateGoogleApiTokens", async (req, res) => {
    const code = req.query.code
    console.log(code)
    const { tokens } = await oauth2Client.getToken(code)
    console.log(tokens)
    saveTokens(tokens)
    Object.assign(gTokens, tokens)
    return res.status(200).send("Sucessfully updated the server's tokens, you may close this page.")
})

const getImageBytes = async (url) => {
    try {
        const response = await axios.get(url, { responseType: "arraybuffer" })
        return response.data
    } catch (error) {
        console.error("Error downloading image:", error)
        throw error
    }
}

const uploadPhoto = async (imageBytes) => {
    const response = await axios.post("https://photoslibrary.googleapis.com/v1/uploads", imageBytes, {
        headers: {
            Authorization: `Bearer ${oauth2Client.credentials.access_token}`,
            "Content-type": "application/octet-stream",
            "X-Goog-Upload-Content-Type": "image/jpeg",
            "X-Goog-Upload-Protocol": "raw",
        },
    })
    return response.data
}

const addPhotosToGoogleAlbum = async (uploadTokens) => {
    if (uploadTokens.length == 0) return

    try {
        // My Dalle3 Image Generations
        const albumId = process.env.MY_DALLE_GOOGLE_ALBUM_ID

        await axios.post(
            "https://photoslibrary.googleapis.com/v1/mediaItems:batchCreate",
            {
                newMediaItems: (await Promise.all(uploadTokens)).map((token) => {
                    return {
                        simpleMediaItem: {
                            uploadToken: token,
                        },
                    }
                }),
                albumId,
            },
            {
                headers: {
                    Authorization: `Bearer ${oauth2Client.credentials.access_token}`,
                    "Content-Type": "application/json",
                },
            },
        )
        if (uploadTokens.length > 12) {
            console.log(`Finished adding ${uploadTokens.length} photos to album!`)
        }
    } catch (error) {
        console.log("Error adding photos to google photo album!")
        console.log(error)
    }
}

const copyPhotoToGoogle = async (imagePath) => {
    const imageBytes = await getImageBytes(imagePath)
    const uploadToken = await uploadPhoto(imageBytes)
    // console.log("Photo Uploaded to Google")
    return uploadToken
}

const refreshAccessTokenIfNeeded = async () => {
    const currentTime = new Date().getTime()
    const isTokenExpired = currentTime >= oauth2Client.credentials.expiry_date

    if (isTokenExpired) {
        console.log("Token is expired!")
        await oauth2Client.refreshAccessToken()
        const updatedTokens = oauth2Client.credentials
        console.log("Saving updated tokens!")
        Object.assign(gTokens, updatedTokens)
        oauth2Client.setCredentials(gTokens)
        saveTokens(gTokens)
    }
}

app.get("/imageDownloadProxy", async (req, res) => {
    try {
        const url = req.query.url
        const response = await axios.get(url, { responseType: "arraybuffer" })
        res.send(response.data)
    } catch (error) {
        res.status(500).send("Error fetching image")
    }
})

app.use(express.static("html"))

WebSocket.prototype.sendJson = function (msg) {
    try {
        this.send(JSON.stringify(msg))
    } catch (error) {
        console.log("Error with JSON stringify or sending message on socket")
        console.log(error)
    }
}

class DalleQueue {
    constructor() {
        this.tasks = []
        this.availableBuffer = 0
    }

    intervalUpdate() {
        if (this.tasks.length > 0 && this.availableBuffer == 60000) {
            this.availableBuffer -= 5000
            const { requestData, resolve, reject } = this.tasks.shift()
            this.processNext(requestData, resolve, reject)
        }
        this.availableBuffer = Math.min(60000, this.availableBuffer + 100)
    }

    async processNext(requestData, resolve, reject) {
        openai.images
            .generate({
                model: "dall-e-3",
                prompt: requestData.prompt,
                size: requestData.resolution,
                quality: requestData.hd ? "hd" : "standard",
            })
            .then(resolve)
            .catch(reject)
    }

    enqueue(requestData) {
        if (this.availableBuffer >= 4000) {
            // Process Immediately
            this.availableBuffer -= 4000
            return new Promise((resolve, reject) => {
                this.processNext(requestData, resolve, reject)
            })
        } else {
            // Not enough credits, add to queue
            return new Promise((resolve, reject) => {
                this.tasks.push({ requestData, resolve, reject })
            })
        }
    }
}

const queue = new DalleQueue()

setInterval(() => {
    queue.intervalUpdate()
}, 100)

wss.on("connection", (socket) => {
    // Set up a 20-second ping interval to keep client connected
    const connectionHeartbeat = setInterval(() => {
        socket.ping()
    }, 20000)

    socket.on("close", () => {
        clearInterval(connectionHeartbeat)
    })

    socket.on("message", async (msgString) => {

        let msg
        let numImages = 0

        try {
            msg = JSON.parse(msgString)
            numImages = parseInt(msg.numImages)
        } catch (error) {
            console.log("Error parsing message from socket client")
            console.log(error)
            socket.sendJson({ error: "Invalid request" })
            return
        }

        let uploadPhotosToGoogleAlbum = msg.saveImages
        try {
            await refreshAccessTokenIfNeeded()
        } catch (error) {
            if (uploadPhotosToGoogleAlbum) {
                uploadPhotosToGoogleAlbum = false
                socket.sendJson({ error: true, googleApiNeedsRefresh: true })
            }
        }

        const uploadTokens = []
        let completedImages = 0

        for (let i = 0; i < numImages; i++) {
            // console.log("Requesting Image", i)

            queue.enqueue(msg).then((image) => {
                    // console.log("Completed Generating Image", i)
                    completedImages++

                    // Upload to Google Photos
                    if (uploadPhotosToGoogleAlbum) {
                        try {
                            const token = copyPhotoToGoogle(image.data[0].url)
                            uploadTokens.push(token)
                        } catch (error) {
                            console.log("Error uploading photo Google photos")
                            console.log(error)
                            // Can continue execution here
                            // This photo will still be sent to socket client just not added to google photo album
                        }
                    }

                    const result = image.data[0]

                    socket.sendJson(result)
                    if (completedImages == numImages) {
                        addPhotosToGoogleAlbum(uploadTokens)
                    }
                })
                .catch((error) => {
                    // console.error("Error Generating Image", i)
                    completedImages++

                    const errorImg = {
                        revised_prompt: error.error.message,
                        url: "https://icon-library.com/images/failed-icon/failed-icon-7.jpg",
                    }

                    socket.sendJson(errorImg)
                    if (completedImages == numImages) {
                        addPhotosToGoogleAlbum(uploadTokens)
                    }
                })
        }
    })
})

server.listen(port, () => {
    console.log(`App listening on port ${port}`)
})
