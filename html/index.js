let numRequestedImages = 0

let socket = new WebSocket(`${window.location}`.replace("http", "ws"))

const socketMessageHandler = (event) => {
    const data = JSON.parse(event.data)

    if (data.error) {
        if (data.googleApiNeedsRefresh) {
            document.getElementById("warningMsg").style.display = "block"
            document.getElementById("warningMsg").innerHTML =
                "Warning: Your photos will not be saved to album. If you want to keep your photos, download them now.<br>Tell Snuffy he needs to refresh the Google Photos API tokens."
        }
        return
    }

    displayImage(data)

    const container = document.getElementById("imageContainer")
    if (container.children.length == numRequestedImages) {
        document.getElementById("loading").style.display = "none"
    }
}

socket.onmessage = socketMessageHandler

document.getElementById("fetchButton").addEventListener("click", () => {
    if (socket && socket.readyState == WebSocket.OPEN) {
        requestImages()
    } else {
        socket = new WebSocket(`${window.location}`.replace("http", "ws"))
        socket.onopen = () => {
            requestImages()
        }
        socket.onmessage = socketMessageHandler
    }
})

const requestImages = () => {
    document.getElementById("warningMsg").style.display = "none"
    document.getElementById("loading").style.display = "block"
    document.getElementById("imageContainer").innerHTML = ""

    const prompt = document.getElementById("promptBox").value
    const resolution = document.getElementById("resolutionSelector").value
    const hd = document.getElementById("hdCheckbox").checked == true
    const numImages = document.getElementById("numImages").value
    const saveImages = document.getElementById("saveCheckbox").checked == true

    numRequestedImages = numImages
    socket.send(JSON.stringify({ prompt, resolution, hd, numImages, saveImages }))
}

const displayImage = (image) => {
    const container = document.getElementById("imageContainer")
    const div = document.createElement("div")
    div.className = "imageItem"
    const img = document.createElement("img")
    img.src = image.url
    img.alt = image.revised_prompt
    const desc = document.createElement("p")
    desc.textContent = image.revised_prompt

    // Create a container for the buttons
    const buttonContainer = document.createElement("div")
    buttonContainer.className = "buttonContainer"

    const downloadButton = document.createElement("button")
    downloadButton.textContent = "Download Full Size"
    downloadButton.className = "button"
    downloadButton.onclick = () => {
        const proxyUrl = `/imageDownloadProxy?url=${encodeURIComponent(image.url)}`
        const tempLink = document.createElement("a")
        tempLink.href = proxyUrl
        tempLink.download = `ImageFromSnuffy-${new Date().getTime().toString()}.jpg`
        document.body.appendChild(tempLink)
        tempLink.click()
        document.body.removeChild(tempLink)
    }

    const variationsButton = document.createElement("button")
    variationsButton.textContent = "More Like This"
    variationsButton.className = "button"
    variationsButton.onclick = () => {
        const promptBox = document.getElementById("promptBox")
        promptBox.value = `Make a variation of this prompt: ${image.revised_prompt}`
    }

    // Append buttons to the button container
    buttonContainer.appendChild(downloadButton)
    buttonContainer.appendChild(variationsButton)

    div.appendChild(img)
    div.appendChild(desc)
    div.appendChild(buttonContainer)
    container.appendChild(div)
}
